# django_api
A port of   
https://www.analyticsvidhya.com/blog/2017/09/machine-learning-models-as-apis-using-flask/   
to Django 2.0

1. Install Django Rest Framework  
http://www.django-rest-framework.org/tutorial/quickstart/

2. Test prediction  
curl --user admin:password123 -i -H "Content-Type: application/json" -X POST -d '[{"Loan_ID":"LP001015","Gender":"Male","Married":"Yes","Dependents":"0","Education":"Graduate","Self_Employed":"No","ApplicantIncome":5720,"CoapplicantIncome":0,"LoanAmount":110.0,"Loan_Amount_Term":360.0,"Credit_History":1.0,"Property_Area":"Urban"}]' http://127.0.0.1:8000/predict/
