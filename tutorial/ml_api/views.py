import pandas as pd
import dill as pickle

from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.decorators import api_view

from tutorial.ml_api.utils import *


@api_view(['GET'])
def test(request):
    """test GET request"""
    return HttpResponse(test_get())


@api_view(['POST'])
def predict(request):
    """API Call
       JSON (sent as a payload) from API Call
    """
    try:
        test_json = request.data
        test_df = pd.DataFrame(test_json)
        # Getting the Loan_IDs separated out
        loan_ids = test_df['Loan_ID']

    except Exception as e:
        raise e

    clf = 'model_v1.pk'

    if test_df.empty:
        return bad_request()
    else:
        print("Loading the model...")
        loaded_model = None
        with open('tutorial/ml_api/model/' + clf, 'rb') as f:
            loaded_model = pickle.load(f)
        print("The model has been loaded...doing predictions now...")

        predictions = loaded_model.predict(test_df)

        """
            Add the predictions as Series to a new pandas dataframe
            OR
            Depending on the use-case, the entire test data appended with the new files
        """
        prediction_series = list(pd.Series(predictions))
        final_predictions = pd.DataFrame(list(zip(loan_ids, prediction_series)))

        """
            We can be as creative in sending the responses.
            But we need to send the response codes as well.
        """
        responses = JsonResponse(final_predictions.to_json(), safe=False)
        responses.status_code = 200

        return HttpResponse(responses)


def bad_request():
    message = {
        'status': 400,
        'message': 'Bad Request: ' + request.url + '--> Please check your data payload...',
    }
    resp = jsonify(message)
    resp.status_code = 400

    # return response
    return HttpResponseBadRequest(resp)
